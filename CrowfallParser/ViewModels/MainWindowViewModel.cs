using System.Diagnostics;
using System.Reflection;
using Prism.Mvvm;

namespace CrowfallParser.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = $"Crowfall Parser {FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion} by Amaranth";


        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
    }
}