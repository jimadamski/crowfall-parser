using System;
using System.Globalization;
using System.Reactive.Subjects;
using System.Windows.Media;
using IniFileIO;
using Prism.Mvvm;

namespace Options
{
    public class Options : BindableBase
    {
        private static Options _inst;
        private readonly IniFile _file;
        public readonly Subject<bool> SaveError = new Subject<bool>();

        private Options()
        {
            _file = new IniFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");
        }

        public static Options Inst => _inst ?? (_inst = new Options());

        public double PrimaryOpacity
        {
            get => Convert.ToDouble(_file.GetKeyValue(Sections.Ui, nameof(PrimaryOpacity), "0.5"), CultureInfo.InvariantCulture);
            set
            {
                _file.SetKeyData(Sections.Ui, nameof(PrimaryOpacity), value.ToString(CultureInfo.InvariantCulture));
                _file.SaveFile();
                RaisePropertyChanged(nameof(PrimaryOpacity));
            }
        }

        public double SecondaryOpacity
        {
            get => Convert.ToDouble(_file.GetKeyValue(Sections.Ui, nameof(SecondaryOpacity), "0.3"), CultureInfo.InvariantCulture);
            set
            {
                _file.SetKeyData(Sections.Ui, nameof(SecondaryOpacity), value.ToString(CultureInfo.InvariantCulture));
                _file.SaveFile();
                RaisePropertyChanged(nameof(SecondaryOpacity));
            }
        }


        public Brush Foreground => new SolidColorBrush(ForegroundColor);

        public Color ForegroundColor
        {
            get => (Color?) ColorConverter.ConvertFromString(_file.GetKeyValue(Sections.Ui, nameof(ForegroundColor), "White")) ?? Colors.White;
            set
            {
                _file.SetKeyData(Sections.Ui, nameof(ForegroundColor), value.ToString());
                _file.SaveFile();
                RaisePropertyChanged(nameof(ForegroundColor));
                RaisePropertyChanged(nameof(Foreground));
            }
        }

        public Color BackgroundColor
        {
            get => (Color?) ColorConverter.ConvertFromString(_file.GetKeyValue(Sections.Ui, nameof(BackgroundColor), "Black")) ?? Colors.Black;
            set
            {
                _file.SetKeyData(Sections.Ui, nameof(BackgroundColor), value.ToString());
                _file.SaveFile();
                RaisePropertyChanged(nameof(BackgroundColor));
            }
        }


        public int FontSize
        {
            get => Convert.ToInt32(_file.GetKeyValue(Sections.Ui, nameof(FontSize), "13"));
            set
            {
                _file.SetKeyData(Sections.Ui, nameof(FontSize), value.ToString());
                _file.SaveFile();
                RaisePropertyChanged(nameof(FontSize));
            }
        }

        public int IdleDuration
        {
            get => Convert.ToInt32(_file.GetKeyValue(Sections.Parser, nameof(IdleDuration), "5"));
            set
            {
                _file.SetKeyData(Sections.Parser, nameof(IdleDuration), value.ToString());
                _file.SaveFile();
                RaisePropertyChanged(nameof(IdleDuration));
            }
        }

        public int NotifierTimer
        {
            get => Convert.ToInt32(_file.GetKeyValue(Sections.AfkNotifier, nameof(NotifierTimer), "9"));
            set
            {
                _file.SetKeyData(Sections.AfkNotifier, nameof(NotifierTimer), value.ToString());
                _file.SaveFile();
                RaisePropertyChanged(nameof(NotifierTimer));
            }
        }

        public double Top
        {
            get => Convert.ToDouble(_file.GetKeyValue(Sections.Ui, nameof(Top), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(Top), value.ToString(CultureInfo.InvariantCulture));
        }

        public double Left
        {
            get => Convert.ToDouble(_file.GetKeyValue(Sections.Ui, nameof(Left), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(Left), value.ToString(CultureInfo.InvariantCulture));
        }

        private void SaveOption(string section, string propertyName, string value)
        {
            try
            {
                _file.SetKeyData(section, propertyName, value);
                _file.SaveFile();
                RaisePropertyChanged(propertyName);
            }
            catch
            {
                SaveError.OnNext(true);
            }
        }

        public void ResetToDefaults()
        {
            ForegroundColor = Colors.White;
            BackgroundColor = Colors.Black;
            PrimaryOpacity = 0.5d;
            SecondaryOpacity = 0.3d;
            FontSize = 13;
            IdleDuration = 5;
            NotifierTimer = 9;
        }
    }

    public static class Sections
    {
        public static string Ui => "UI";
        public static string Parser => "Parser";
        public static string AfkNotifier => "AfkNotifier";
    }
}