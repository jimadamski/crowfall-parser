using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using Skills.Models;
using Skills.Services;

namespace Skills.ViewModels
{
    public class MetricsViewModel : BindableBase
    {
        private readonly DispatcherTimer _characterSelector = new DispatcherTimer {Interval = TimeSpan.FromSeconds(10)};
        private readonly FightService _fightService;
        private readonly List<CombatLog> _logs = new List<CombatLog>();
        private readonly DispatcherTimer _parser = new DispatcherTimer {Interval = TimeSpan.FromSeconds(1)};
        private readonly DispatcherTimer _watcher = new DispatcherTimer {Interval = TimeSpan.FromSeconds(5)};
        private DirectoryInfo _directoryInfo;
        private Fight _fight;
        private int _fightsCount;
        private bool _notifierEnabled;
        private Visibility _showDetails = Visibility.Collapsed;
        private Visibility _showOpponents = Visibility.Collapsed;
        private Visibility _showSaveError = Visibility.Collapsed;
        private Visibility _showSettings = Visibility.Collapsed;

        public MetricsViewModel(FightService fightService, NotifierService notifierService)
        {
            Options.Options.Inst.SaveError.Subscribe(next => ShowSaveError = Visibility.Visible);
            _fightService = fightService;

            DamageSkillsListModel = new SkillListModel {TotalLabel = "Damage", PerSecondLabel = "DPS"};
            HealSkillsListModel = new SkillListModel {TotalLabel = "Healing", PerSecondLabel = "HPS"};

            _parser.Tick += ParserOnTick;

            _watcher.Tick += AttachToProcess;
            _watcher.Start();

            _characterSelector.Tick += _fightService.SelectCharacter;
            _characterSelector.Start();

            ShowSettingsCommand = new DelegateCommand(() => ShowSettings = ShowSettings == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible);
            ShowDetailsCommand = new DelegateCommand(() => ShowDetails = ShowDetails == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible);
            SwitchNotifierCommand = new DelegateCommand(() =>
            {
                NotifierEnabled = !NotifierEnabled;
                if (NotifierEnabled)
                    notifierService.StartListeningForWindowChanges();
                else
                    notifierService.StopListeningForWindowChanges();
            });
            FirstFightCommand = new DelegateCommand(() =>
            {
                _fightService.First();
                UpdateFight();
            });
            LastFightCommand = new DelegateCommand(() =>
            {
                _fightService.Last();
                UpdateFight();
            });
            PreviousFightCommand = new DelegateCommand(() =>
            {
                _fightService.Previous();
                UpdateFight();
            });
            NextFightCommand = new DelegateCommand(() =>
            {
                _fightService.Next();
                UpdateFight();
            });
            RecalculateCommand = new DelegateCommand(() =>
            {
                _fightService.Reset();
                foreach (var log in _logs)
                {
                    log.Reader.BaseStream.Position = 0;
                    while (!log.Reader.EndOfStream)
                    {
                        var line = log.Reader.ReadLine();
                        if (line != null)
                        {
                            _fightService.ParseLine(log.FileInfo.Name, line);
                        }
                    }
                }

                UpdateFight();
            });
            ResetOptionsCommand = new DelegateCommand(() => Options.Options.Inst.ResetToDefaults());
            ShowOpponentsCommand = new DelegateCommand(() => ShowOpponents = ShowOpponents == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible);
            DonateCommand = new DelegateCommand(() => Process.Start(new ProcessStartInfo(DonationLink)));
        }

        public Fight Fight
        {
            get => _fight;
            private set
            {
                SetProperty(ref _fight, value);
                RaisePropertyChanged(nameof(Duration));
                RaisePropertyChanged(nameof(CriticalRate));
            }
        }

        public Visibility ShowSaveError
        {
            get => _showSaveError;
            set => SetProperty(ref _showSaveError, value);
        }

        public Visibility ShowSettings
        {
            get => _showSettings;
            private set => SetProperty(ref _showSettings, value);
        }

        public Visibility ShowDetails
        {
            get => _showDetails;
            private set => SetProperty(ref _showDetails, value);
        }

        public Visibility ShowOpponents
        {
            get => _showOpponents;
            private set => SetProperty(ref _showOpponents, value);
        }

        public bool NotifierEnabled
        {
            get => _notifierEnabled;
            private set => SetProperty(ref _notifierEnabled, value);
        }

        public int FightsCount
        {
            get => _fightsCount;
            set => SetProperty(ref _fightsCount, value);
        }

        public string CriticalRate => $"{Fight.CriticalRate:0}%";
        public string Duration => $"{Fight.Duration / 1000:0.#}s";

        public SkillListModel DamageSkillsListModel { get; set; }

        public SkillListModel HealSkillsListModel { get; set; }

        public DelegateCommand ShowSettingsCommand { get; }
        public DelegateCommand ShowDetailsCommand { get; }
        public DelegateCommand SwitchNotifierCommand { get; }

        public DelegateCommand PreviousFightCommand { get; }

        public DelegateCommand NextFightCommand { get; }

        public DelegateCommand LastFightCommand { get; }

        public DelegateCommand FirstFightCommand { get; }
        public DelegateCommand RecalculateCommand { get; }

        public DelegateCommand ResetOptionsCommand { get; }
        public DelegateCommand ShowOpponentsCommand { get; }
        public DelegateCommand DonateCommand { get; }

        public string DonationLink => "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=4YSB8PYHPNWC2&source=url";

        private void ParserOnTick(object sender, EventArgs e)
        {
            UpdateLogsList();
            foreach (var log in _logs)
            {
                while (!log.Reader.EndOfStream)
                {
                    var line = log.Reader.ReadLine();
                    if (line != null)
                    {
                        _fightService.ParseLine(log.FileInfo.Name, line);
                    }
                }
            }

            UpdateFight();
        }

        private void UpdateFight()
        {
            if (_fightService.CurrentFight == null)
                return;

            Fight = _fightService.CurrentFight;
            DamageSkillsListModel.Skills = Fight.DamageDoneRecords;
            HealSkillsListModel.Skills = Fight.HealingDoneRecords;
            FightsCount = _fightService.FightsCount;
        }

        private void UpdateLogsList()
        {
            var anHourAgo = DateTime.Now.Subtract(TimeSpan.FromHours(1));
            foreach (var fileInfo in _directoryInfo.GetFiles().Where(x => x.LastWriteTime > anHourAgo))
            {
                if (_logs.All(x => x.FileInfo.FullName != fileInfo.FullName))
                    _logs.Add(new CombatLog(fileInfo));
            }
        }

        private void AttachToProcess(object sender, EventArgs e)
        {
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var logsDirectory = appData + @"Low\Art+Craft\Crowfall\CombatLogs";

            _directoryInfo = new DirectoryInfo(logsDirectory);
            if (!_directoryInfo.Exists)
            {
                var fileDialog = new OpenFileDialog {Title = "Open any Crowfall combat log", Filter = "*.log|*.*", DefaultExt = "*.log"};
                if (fileDialog.ShowDialog() != true)
                {
                    MessageBox.Show("Parser could not find combat logs. Please enable \"Write combat log to file\" in Crowfall settings, hit something and start parser again.",
                        "Closing... See you soon!");
                    Application.Current.Shutdown();
                    return;
                }

                var directoryName = Path.GetDirectoryName(fileDialog.FileName);
                if (directoryName != null)
                    _directoryInfo = new DirectoryInfo(directoryName);
            }

            foreach (var fileInfo in _directoryInfo.GetFiles().OrderByDescending(x => x.LastWriteTime).Take(5))
            {
                _logs.Add(new CombatLog(fileInfo));
            }

            if (!_logs.Any())
                return;

            _parser.Start();
            _watcher.Stop();
        }
    }

    public class Opponent
    {
        public Opponent(string name, int damageDone)
        {
            Name = name;
            DamageDone = damageDone;
        }

        public string Name { get; }
        public int DamageDone { get; set; }
    }

    internal class CombatLog
    {
        public CombatLog(FileInfo fileInfo)
        {
            FileInfo = fileInfo;
            Reader = new StreamReader(FileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
        }

        public FileInfo FileInfo { get; }
        public StreamReader Reader { get; }
    }
}