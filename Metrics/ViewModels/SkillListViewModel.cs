using Prism.Mvvm;
using Skills.Models;

namespace Skills.ViewModels
{
    public class SkillListViewModel : BindableBase
    {
        private SkillListModel _model;

        public SkillListModel Model
        {
            get { return _model; }
            set { SetProperty(ref _model, value); }
        }
    }
}