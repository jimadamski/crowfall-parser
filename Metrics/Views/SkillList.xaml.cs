﻿using Prism.Common;
using Prism.Regions;
using Skills.Models;
using Skills.ViewModels;

namespace Skills.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SkillList
    {
        public SkillList()
        {
            InitializeComponent();
            RegionContext.GetObservableContext(this).PropertyChanged += SkillList_PropertyChanged;
        }

        private void SkillList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var context = (ObservableObject<object>) sender;
            var model = (SkillListModel) context.Value;
            ((SkillListViewModel) DataContext).Model = model;
        }
    }
}