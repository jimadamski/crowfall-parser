using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Skills.Annotations;
using Skills.Models;

namespace Skills.Services
{
    public sealed class FightService : INotifyPropertyChanged
    {
        private readonly List<Character> _characters = new List<Character>();
        private Character _currentCharacter;

        private Character CurrentCharacter
        {
            get => _currentCharacter;
            set
            {
                _currentCharacter = value;
                OnPropertyChanged(nameof(CurrentCharacter));
                OnPropertyChanged(nameof(CurrentFight));
                OnPropertyChanged(nameof(FightsCount));
            }
        }

        public Fight CurrentFight => CurrentCharacter?.CurrentFight;

        public int FightsCount => CurrentCharacter?.Fights.Count ?? 0;

        public event PropertyChangedEventHandler PropertyChanged;

        public void ParseLine(string character, string line)
        {
            var match = new Regex(@"(.*) INFO    COMBAT    - Combat _\|\|_ Event=\[(Your|\w*)(.*) (hit|healed|whiffed) (You|.*) for (\d*)(.*)\.\]").Match(line);
            if (!match.Success)
                return;

            var timestamp = DateTime.Parse(match.Groups[1].Value);
            var currentFight = SolveCurrentFight(character, timestamp);

            if (currentFight == null)
                return;

            var damage = match.Groups[4].Value != "healed";
            var skillName = match.Groups[3].Value.Trim();
            var skill = string.IsNullOrEmpty(skillName) ? (damage ? "Unknown" : "Lifesteal") : skillName;
            var mine = match.Groups[2].Value == "Your" && skill != "Fall" && skill != "Berserk"; // todo: translations?
            var target = match.Groups[5].Value;
            var me = target == "You";
            var amount = int.Parse(match.Groups[6].Value);
            var critical = match.Groups[7].Value.Contains("(Critical)");

            if (mine)
            {
                if (damage)
                {
                    currentFight.UpdateDamageDone(skill, target, amount, critical);

                    if (me)
                        currentFight.UpdateDamageReceived(skill, amount, critical);
                }
                else
                {
                    currentFight.UpdateHealingDone(skill, amount, critical);

                    if (me)
                        currentFight.UpdateHealingReceived(skill, amount, critical);
                }
            }
            else
            {
                if (damage)
                    currentFight.UpdateDamageReceived(skill, amount, critical);
                else
                    currentFight.UpdateHealingReceived(skill, amount, critical);
            }

            currentFight.UpdatePerSecond();
        }

        private Fight SolveCurrentFight(string id, DateTime timestamp)
        {
            var character = _characters.FirstOrDefault(x => x.Id == id);

            if (character == null)
            {
                character = new Character(id);
                _characters.Add(character);
            }

            var fights = character.Fights;
            if (character.CurrentFight == null)
            {
                character.CurrentFight = new Fight {Started = timestamp, Index = fights.Count + 1};
                fights.Add(character.CurrentFight);
            }
            else if (timestamp - character.CurrentFight.LastAction > TimeSpan.FromSeconds(Options.Options.Inst.IdleDuration))
            {
                var i = fights.IndexOf(character.CurrentFight);
                while (i < fights.Count && timestamp - fights[i].LastAction > TimeSpan.FromSeconds(Options.Options.Inst.IdleDuration))
                {
                    i++;
                }

                if (i == fights.Count)
                {
                    character.CurrentFight = new Fight {Started = timestamp, Index = fights.Count + 1};
                    fights.Add(character.CurrentFight);
                }
                else
                {
                    character.CurrentFight = fights[i];
                }
            }

            character.CurrentFight.LastAction = timestamp;

            if (CurrentCharacter == null)
                CurrentCharacter = character;

            return character.CurrentFight;
        }

        public void First()
        {
            if (CurrentFight != null)
                _currentCharacter.CurrentFight = _currentCharacter.Fights.FirstOrDefault();

            OnPropertyChanged(nameof(CurrentFight));
        }

        public void Last()
        {
            if (CurrentFight != null)
                _currentCharacter.CurrentFight = _currentCharacter.Fights.LastOrDefault();

            OnPropertyChanged(nameof(CurrentFight));
        }

        public void Next()
        {
            if (CurrentFight == null)
                return;

            var fights = _currentCharacter.Fights;
            var nextFight = fights.ElementAtOrDefault(fights.IndexOf(CurrentFight) + 1);

            if (nextFight != null)
                _currentCharacter.CurrentFight = nextFight;

            OnPropertyChanged(nameof(CurrentFight));
        }

        public void Previous()
        {
            if (CurrentFight == null)
                return;

            var fights = _currentCharacter.Fights;
            var previousFight = fights.ElementAtOrDefault(fights.IndexOf(CurrentFight) - 1);

            if (previousFight != null)
                _currentCharacter.CurrentFight = previousFight;

            OnPropertyChanged(nameof(CurrentFight));
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Reset()
        {
            _currentCharacter = null;
            _characters.Clear();
        }

        public void SelectCharacter(object sender, EventArgs e)
        {
            var bestActivity = -1;
            Character mostActiveCharacter = null;
            var tenSecondsAgo = DateTime.Now.Subtract(TimeSpan.FromSeconds(10));
            foreach (var character in _characters)
            {
                var fights = character.Fights.Where(x => x.LastAction > tenSecondsAgo);
                var activity = fights.Select(x => x.DamageDone + x.HealingDone).Sum();
                if (activity > bestActivity)
                {
                    bestActivity = activity;
                    mostActiveCharacter = character;
                }
            }

            if (bestActivity > 0)
                CurrentCharacter = mostActiveCharacter;
        }
    }

    public class Character
    {
        public Character(string id)
        {
            Id = id;
        }

        public List<Fight> Fights { get; } = new List<Fight>();
        public Fight CurrentFight { get; set; }
        public string Id { get; }
    }
}