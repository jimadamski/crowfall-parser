using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Skills.Convertors
{
    public class ShowDetailsToMoreLabelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && (Visibility) value == Visibility.Visible ? "⮝ More" : "⮟ More";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}