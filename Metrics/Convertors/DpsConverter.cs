using System;
using System.Globalization;
using System.Windows.Data;

namespace Skills.Convertors
{
    public class DpsConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var totalDamage = (int) values[0];
            var duration = (int) values[1];
            return duration == 0 ? "" : (totalDamage / duration).ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}