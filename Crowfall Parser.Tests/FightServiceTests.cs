﻿using System;
using System.Linq;
using Skills.Extensions;
using Skills.Services;
using Xunit;

namespace Crowfall_Parser.Tests
{
    public class Tests
    {
        public Tests()
        {
            _fightService = new FightService();
        }

        private readonly FightService _fightService;

        [Fact]
        public void TestDamageLine()
        {
            _fightService.ParseLine("", "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Your Smash hit Practice Dummy for 500  damage.] ");
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(2, _fightService.CurrentFight.DamageDoneRecords.Count);

            _fightService.ParseLine("", "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Your Smash hit Practice Dummy for 1500  damage.] ");
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(2, _fightService.CurrentFight.DamageDoneRecords.Count);
            var total = _fightService.CurrentFight.DamageDoneRecords.FirstOrDefault(x => x.Name == "Total");
            Assert.NotNull(total);
            Assert.Equal(2000, total.Total);
            Assert.Equal(1000, total.PerSecond);
        }

        [Fact]
        public void TestHealLine()
        {
            _fightService.ParseLine("", "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Your Ultimate Warrior healed You for 2000 hit points.] ");
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(2, _fightService.CurrentFight.HealingDoneRecords.Count);

            _fightService.ParseLine("", "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Your Ultimate Warrior healed You for 4000 hit points.] ");
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(2, _fightService.CurrentFight.HealingDoneRecords.Count);
            var total = _fightService.CurrentFight.HealingDoneRecords.FirstOrDefault(x => x.Name == "Total");
            Assert.NotNull(total);
            Assert.Equal(6000, total.Total);
            Assert.Equal(3000, total.PerSecond);
        }

        [Fact]
        public void TestReceivedLines()
        {
            _fightService.ParseLine("", "2019-01-30T11:00:00.000Z INFO    COMBAT    - Combat _||_ Event=[Muskhog Berserker Gore hit You for 48  damage.] ");
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(0));
            Assert.Equal(2, _fightService.CurrentFight.DamageReceivedRecords.Count);

            _fightService.ParseLine("", "2019-01-30T11:00:02.000Z INFO    COMBAT    - Combat _||_ Event=[Your Ultimate Warrior healed You for 4000 hit points.] ");
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:00.000Z"), _fightService.CurrentFight.Started);
            Assert.Equal(DateTime.Parse("2019-01-30T11:00:02.000Z"), _fightService.CurrentFight.LastAction);
            Assert.True(_fightService.CurrentFight.Duration.Equal(2000));
            Assert.Equal(2, _fightService.CurrentFight.HealingReceivedRecords.Count);
            var total = _fightService.CurrentFight.HealingReceivedRecords.FirstOrDefault(x => x.Name == "Total");
            Assert.NotNull(total);
            Assert.Equal(4000, total.Total);
            Assert.Equal(2000, total.PerSecond);
        }
    }
}